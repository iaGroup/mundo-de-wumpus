/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mundowumpus;

import java.util.Random;
import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author andre
 */
public class MundoWumpus {

    static Jogo[][] labirinto;
    static int n;
    static boolean achou = false;

    public static void main(String[] args) throws IOException {

        while (achou == false) {

            Scanner sc = new Scanner(System.in);

            System.out.print("Iniciando o mundo ordem 4");
            n = 4;
            /* CRIANDO TAB DO JOGO */
            labirinto = new Jogo[n][n];
            for (int i = 0; i < n; i++) {
                labirinto[i] = new Jogo[n];
                for (int j = 0; j < n; j++) {
                    labirinto[i][j] = new Jogo();
                }
            }
            Random gerador = new Random();
            int buraco = gerador.nextInt(5);//nivel de dificuldade pelo numero de buracos
            System.out.print("\nSao " + buraco + " buracos\n");
            int xBuraco, yBuraco;
            /* SORTEANDO A POSICAO DOS BURACOS */
            for (int i = 0; i < buraco; i++) {
                xBuraco = gerador.nextInt(3) + 1;
                yBuraco = gerador.nextInt(3) + 1;
                addBuraco(xBuraco, yBuraco);
                System.out.print("Buraco de numero:" + i + " X:" + xBuraco + " Y:" + yBuraco + "\n");
            }

            // System.out.print("\nEntre com a posicao do Wumpus\n");
            /* SORTEANDO POSICAO DO BICHO RUIM */
            int xWumpus = gerador.nextInt(3) + 1;
            int yWumpus = gerador.nextInt(3) + 1;
            System.out.print("Wumpus posicao:" + " X:" + xWumpus + " Y:" + yWumpus);

            addWumpus(n - xWumpus, yWumpus - 1);
            /* SORTEANDO POSICAO DO OURO */
            // System.out.print("\nEntre com a posicao do ouro\n");
            int xOuro = gerador.nextInt(3) + 1;
            int yOuro = gerador.nextInt(3) + 1;
            System.out.print("Ouro posicao:" + " X:" + xOuro + " Y:" + yOuro);

            addOuro(n - xOuro, yOuro - 1);

            System.out.print("\nSaindo da posicao 1 1\n");
            int r = 0;
            int c = 0;
            int rPrev = -1, cPrev = -1;

            int acao = 0;
            System.out.println("\nEstado inicial");
            printlabirinto(r, c);
            /* ENQUANTO NAO ACHAR O OURO OU ACABAR AS POSICOES POSIVEIS FACA VARREDURA */
            while (!labirinto[r][c].temOuro) {
                labirinto[r][c].jaPassei = true;
                labirinto[r][c].buracoStatus = Jogo.NaoEsta; // NAO TEM BURACO
                labirinto[r][c].wumpusStatus = Jogo.NaoEsta; // NAO TEM O BICHO RUIM

                /* LOGICA PARA VERIFICACAO DE PERIGO MARCANDO QUE NADA DE RUIM ESTA NESSA POSICAO DE ACORDO COM O FEDOR OU BRISA
                 GERANDO A BASE DE CONHECIMENTO PARA DESCOBRIR UM NOVO CAMINHO
                PODE SER CARACTERIZADO COMO O AGENTE
                */
                if (!labirinto[r][c].brisa) {
                    if (r >= 1 && labirinto[r - 1][c].buracoStatus == Jogo.perigo) {
                        labirinto[r - 1][c].buracoStatus = Jogo.NaoEsta;
                    }
                    if (r <= (n - 2) && labirinto[r + 1][c].buracoStatus == Jogo.perigo) {
                        labirinto[r + 1][c].buracoStatus = Jogo.NaoEsta;
                    }
                    if (c >= 1 && labirinto[r][c - 1].buracoStatus == Jogo.perigo) {
                        labirinto[r][c - 1].buracoStatus = Jogo.NaoEsta;
                    }
                    if (c <= (n - 2) && labirinto[r][c + 1].buracoStatus == Jogo.perigo) {
                        labirinto[r][c + 1].buracoStatus = Jogo.NaoEsta;
                    }
                }

                if (!labirinto[r][c].fedor) {
                    if (r >= 1 && labirinto[r - 1][c].wumpusStatus == Jogo.perigo) {
                        labirinto[r - 1][c].wumpusStatus = Jogo.NaoEsta;
                    }
                    if (r <= (n - 2) && labirinto[r + 1][c].wumpusStatus == Jogo.perigo) {
                        labirinto[r + 1][c].wumpusStatus = Jogo.NaoEsta;
                    }
                    if (c >= 1 && labirinto[r][c - 1].wumpusStatus == Jogo.perigo) {
                        labirinto[r][c - 1].wumpusStatus = Jogo.NaoEsta;
                    }
                    if (c <= (n - 2) && labirinto[r][c + 1].wumpusStatus == Jogo.perigo) {
                        labirinto[r][c + 1].wumpusStatus = Jogo.NaoEsta;
                    }
                }

                boolean NovoCaminho = false;
                /* 
                MOTOR DE INFERENCIA
                PROCURANDO NOVO CAMINHO DE SEGUINDO SE JA PASSOU OU SE TEM PERIGO 
                Sequencia de escolhas alinhadas que são aplicadas a partir da base
                de conhecimento.
                */
                
                if (r >= 1 && !((r - 1) == rPrev && c == cPrev) && labirinto[r - 1][c].jaPassei == false && labirinto[r - 1][c].buracoStatus == Jogo.NaoEsta && labirinto[r - 1][c].wumpusStatus == Jogo.NaoEsta) {
                    rPrev = r;
                    cPrev = c;

                    r--;
                    NovoCaminho = true;
                } else if (r <= (n - 2) && !((r + 1) == rPrev && c == cPrev) && labirinto[r + 1][c].jaPassei == false && labirinto[r + 1][c].buracoStatus == Jogo.NaoEsta && labirinto[r + 1][c].wumpusStatus == Jogo.NaoEsta) {
                    rPrev = r;
                    cPrev = c;

                    r++;
                    NovoCaminho = true;
                } else if (c >= 1 && !(r == rPrev && (c - 1) == cPrev) && labirinto[r][c - 1].jaPassei == false && labirinto[r][c - 1].buracoStatus == Jogo.NaoEsta && labirinto[r][c - 1].wumpusStatus == Jogo.NaoEsta) {
                    rPrev = r;
                    cPrev = c;

                    c--;
                    NovoCaminho = true;
                } else if (c <= (n - 2) && !(r == rPrev && (c + 1) == cPrev) && labirinto[r][c + 1].jaPassei == false && labirinto[r][c + 1].buracoStatus == Jogo.NaoEsta && labirinto[r][c + 1].wumpusStatus == Jogo.NaoEsta) {
                    rPrev = r;
                    cPrev = c;

                    c++;
                    NovoCaminho = true;
                }
                /* CASO NAO TIVER CAMINHO ELE VOLTA */
                if (!NovoCaminho) {
                    int temp1 = rPrev;
                    int temp2 = cPrev;

                    rPrev = r;
                    cPrev = c;

                    r = temp1;
                    c = temp2;
                }

                acao++;

                System.out.println("\n\nMovimento " + acao + ":");
                printlabirinto(r, c);
                /* CASO O NUMERO DE ACOES FOR MAIOR QUE O NUMERO DE POSICOES ELE DA COMO PERDIDO*/
                if (acao > n * n) {
                    System.out.println("\n Deu ruim! (nao achou o ouro)");
                    achou = false;
                    salvaDados(acao,-1000,achou);
                    break;
                }
            }

            if (acao <= n * n) {
                System.out.println("\nDeu bom (achou o ouro) " + acao + " acao.");
                achou = true;
                salvaDados(acao,1000,achou);
            }
            
            sc.close();
        }
    }

    static void salvaDados(int acao,int pontuacao, boolean achou) throws IOException {
        if(achou == true){
            acao *= -1;
            try(BufferedWriter buffWrite = new BufferedWriter(new FileWriter("dados.txt", true))){
            buffWrite.append("Ganhou: Acoes:" + acao + " Pontuacao:" + pontuacao + "\n");
            buffWrite.close();
            }
        }else{
            acao *= -1;
            try(BufferedWriter buffWrite = new BufferedWriter(new FileWriter("dados.txt", true))){
            buffWrite.append("Perdeu: Acoes:" + acao + pontuacao + "\n");
            buffWrite.close();
            }
        }
        System.out.println("salvo");
    }

    static void addWumpus(int r, int c) {
        labirinto[r][c].wumpusAqui = true;

        if (r >= 1) {
            labirinto[r - 1][c].fedor = true;
        }
        if (r <= (n - 2)) {
            labirinto[r + 1][c].fedor = true;
        }
        if (c >= 1) {
            labirinto[r][c - 1].fedor = true;
        }
        if (c <= (n - 2)) {
            labirinto[r][c + 1].fedor = true;
        }
    }

    static void addBuraco(int r, int c) {
        labirinto[r][c].buraco = true;

        if (r >= 1) {
            labirinto[r - 1][c].brisa = true;
        }
        if (r <= (n - 2)) {
            labirinto[r + 1][c].brisa = true;
        }
        if (c >= 1) {
            labirinto[r][c - 1].brisa = true;
        }
        if (c <= (n - 2)) {
            labirinto[r][c + 1].brisa = true;
        }
    }

    static void printlabirinto(int r, int c) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                char charToPrint = '-';
                if (r == i && c == j) {
                    charToPrint = '*';
                } else if (labirinto[i][j].buraco) {
                    charToPrint = 'O';
                } else if (labirinto[i][j].wumpusAqui) {
                    charToPrint = 'X';
                } else if (labirinto[i][j].temOuro) {
                    charToPrint = '$';
                }

                System.out.print(charToPrint + "\t");
            }
            System.out.println();
        }
    }

    static void addOuro(int r, int c) {
        labirinto[r][c].temOuro = true;
    }

}
